# Macro Repository

This is a placeholder repository for macro releases. Idea has been that macro
repository is a submodule that contains marco releases as submodules from filesystem repo.
Macro releases are:
  1. Large in size
  2. Often contain process sensitive data, 
  
So they prefereebly should not not be storedon generally accessible web servers. 
Furthermore this arrangement makes it possible to bundle up related macros 
and have development forum for them with issues, merge requests etc. Macros
themselves are currently tbz2 packed file archives.

