#!/usr/bin/env bash
#Selective initialization of submodules
#Written by by Marko Kosunen, marko.kosunen@aalto.fi, 2017
DIR=$( cd `dirname $0` && pwd )
cd $DIR
#List the submodules here
MODULES="\
"

#Self-made selective recursion
echo "Initiating submodules in $DIR"
git submodule sync
for mod in ${MODULES}; do
    git submodule update --init $mod \
        && cd ${mod} || exit 1
    if [ -f ./init_submodules.sh ]; then
        ./init_submodules.sh || exit 1
    fi
    cd ${DIR}

done
exit 0

